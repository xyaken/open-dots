set number
syntax on
set tabstop=4
"自動インデントに使用される空白の数
set shiftwidth=4
"[検索関係]
"大文字小文字の区別なし
set ignorecase
"検索でファイルの末尾に達したら、先頭へループする
set wrapscan
"検索結果をハイライト
set hlsearch

autocmd FileType scheme :let is_gouche=1
autocmd FileType scheme :set autoindent

