alias ls="ls --color=auto"
alias vi="vim"

alias miv="vim -c NERDTree -c 'winc l' -c q"

# cd.コマンドを作る
# alias cd.="cd ../"
# といった感じ。
# cdの次にある"."の個数だけ上のディレクトリに上がる
# 第一引数(MAX):aliasを作る個数
function cd_aliases() {
  local MAX=$1
  local CD="cd"
  local PARENT=""
  
  for i in $(seq 1 $MAX)
  do
    CD="$CD."
    PARENT="$PARENT../"
    alias $CD="cd $PARENT"
  done
}
cd_aliases 10

function yardgraph() {
  local target=$1
  yard graph --full -d | dot -Tpng -o  $target.png
}

function findpr() {
  git log --merges --oneline --reverse --ancestry-path $1...HEAD | grep 'pull request #' | head -n 1
}
