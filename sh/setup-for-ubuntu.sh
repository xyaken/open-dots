#!/bin/bash

echo "tmuxとcurlインストール"
sudo apt-get install -y tmux curl

echo "/etc/apt/sources.listの置き換え"
sudo cp -a /etc/apt/sources.list{,.back}
sudo sed -i 's/^# deb-src/deb-src/' /etc/apt/sources.list

echo "vimコンパイルに必要なものをインストール"
./setup-vim.sh -i

echo "NeoBundleインストール"
curl https://raw.githubusercontent.com/Shougo/neobundle.vim/master/bin/install.sh | sh
