#!/bin/sh

############################################################
# Rsenseのインストール
# 参考元：http://qiita.com/amaranthine/items/edfaf9edc02e606f92d2
#         Rsense(rsense/rsense)を利用してVimでRubyの補完を実現するa - Qiita
# 
############################################################

# gemがインストールされてなければインストール。あればアップデート
gem_install_or_update() {
  local gem=$1
  if [ -z "$( gem list | grep ${gem} )" ] ; then
    gem install ${gem}
  else
    gem update ${gem}
  fi
}

gem_install_or_update rsense
gem_install_or_update rubocop
gem_install_or_update refe2

# 参考元：http://qiita.com/mogulla3/items/42a7f6c73fa4a90b1df3
# リファレンスの構築
bitclust setup

sudo apt-get -y install ctags

which rsense
