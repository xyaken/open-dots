#!/bin/bash

while getopts hacmi opt
do
  case $opt in
    [ai])
      CONFIGURE=1
      MAKE=1
      INSTALL=1
      ;;
    m)
      CONFIGURE=1
      MAKE=1
      ;;
    c)
      CONFIGURE=1
      ;;
    h)
      echo "-a            : 全て実行してインストールする"
      echo "-i            : -aと同じ"
      echo "-m            : makeまで行って、インストールはしない"
      echo "-c            : configureまで行って、make、インストールはしない"
      echo "オプション無し: ソースコードの取得まで。configureもしない"
      exit 1
      ;;
    \?)
      echo "不正なオプション。ヘルプは-hオプション"
      exit 1
      ;;
  esac
done

############################################################
#
# 参考元：http://vim-jp.org/docs/build_linux.html
#         vim-jp » Vimのユーザーと開発者を結ぶコミュニティサイト
#         Linuxでのビルド方法
# 
############################################################

############################################################
# 1. コンパイルに必要なパッケージのインストール
############################################################

echo "開始:必要パッケージのインストール"
# Vimをコンパイルするのに必要なパッケージをインストール
sudo apt-get -y build-dep vim

# GVim(GNOME)をビルドする場合は、以下のパッケージも必要
# sudo apt-get install libxmu-dev libgnomeui-dev libxpm-dev

# GVim(GTK2)をビルドする場合は、以下のパッケージも必要
# Unity(Ubuntu)の場合はこちらが推奨
# sudo apt-get install libxmu-dev libgtk2.0-dev libxpm-dev

# Perl, Python, Ruby拡張を使う場合に必要
sudo apt-get -y install libperl-dev python-dev python3-dev ruby-dev

# Lua拡張を使う場合に必要
sudo apt-get -y install lua5.2 liblua5.2-dev

# LuaJITのLua拡張を使う場合に必要
sudo apt-get -y install luajit libluajit-5.1

# コードを修正する場合に必要になるかも知れないパッケージ
# sudo apt-get install autoconf automake cproto

echo "終了:必要パッケージのインストール"
############################################################
# 2. ソースコードの取得
############################################################
echo "開始:ソースコードの取得"

current_dir=$(dirname $0)
cd ${current_dir}/../

# コード格納用ディレクトリの存在チェックなど
if [ -d work ] ; then
  cd work
elif [ -e work ] ; then
  echo "Error: コード格納用ディレクトリと同名のファイルがある"
  exit 1
else
  mkdir work
  cd work
fi

# .gitディレクトリがある==git clone済ということでgit pullする
if [ -d .git ] ; then
  git pull
else
  # cloneはカレントディレクトリに
  git clone https://github.com/vim/vim.git .
fi

echo "終了:ソースコードの取得"
############################################################
# 3. コンパイル
############################################################
echo "開始:コンパイル"

if [ -z $CONFIGURE ] ; then
  echo "終了:configure手前まで実行"
  exit
fi

echo "開始: configure"
./configure  \
  --with-features=huge --enable-fail-if-missing \
  --enable-perlinterp --enable-pythoninterp \
  --enable-python3interp \
  --enable-rubyinterp \
  --enable-luainterp --with-luajit \
  --enable-multibyte --enable-cscope \
  --prefix=/usr/local

if [ -z $MAKE ] ; then
  echo "終了:make手前まで実行"
  exit
fi

echo "開始: make"
# TODO make と make reconfigの切り替え
make

if [ -z $INSTALL ] ; then
  echo "終了:インストール手前まで実行"
  exit
fi

echo "開始: make install"
sudo make install
