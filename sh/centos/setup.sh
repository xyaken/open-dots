#!/bin/bash

# コード格納用ディレクトリ
mkdir ~/src

# 必要パッケージなどのインストール
sudo yum install -y epel-release
sudo sed -i 's/enabled=1/enabled=0/' /etc/yum.repos.d/epel.repo
sudo yum install -y git tig nodejs --enablerepo=epel
sudo yum install -y gcc ncurses-devel


#---------------------------
# Vim
#---------------------------
sudo yum install -y python34-devel python-devel ruby-devel --enablerepo=epel
sudo yum install -y lua lua-devel luajit luajit-devel --enablerepo=epel

git clone https://github.com/vim/vim.git ~/src/vim

cd ~/src/vim
# CFLAGS="-I/usr/lib64/perl5/CORE/ -Doff64_t=__off64_t" について
# これがないと--enable-perlinterpを指定した時にエラーになる。
# 「if_perl.xs:57:20: 致命的エラー: EXTERN.h: そのようなファイルやディレクトリはありません」
# といったエラーがmake時に出る
# さらに
# 「エラー: 不明な型名 ‘off64_t’ です」
# 「ERROR: Unknown type name 'off64_t'」
# というエラーを抑止するため 「-Doff64_t=...」を指定している
# しかし、上記の指定をしてもエラーになるので--enable-perlinterpを付けるのを諦めた。
# perlinterpを使う場合、yum install perl-develをする必要もあるので忘れないように。
sudo ./configure  \
  --with-features=huge --enable-fail-if-missing \
  --enable-pythoninterp \
  --enable-python3interp \
  --enable-rubyinterp \
  --enable-luainterp --with-luajit \
  --enable-multibyte --enable-cscope \
  --prefix=/usr/local

sudo make

sudo make install

#---------------------------
# tmux
#---------------------------

cd ~/src
sudo yum install -y libevent-devel
curl -kLO https://github.com/tmux/tmux/releases/download/2.3/tmux-2.3.tar.gz 
tar zxf tmux-2.3.tar.gz
cd tmux-2.3
./configure
make
sudo make install

#---------------------------
# rbenv
#---------------------------
git clone https://github.com/sstephenson/rbenv.git ~/.rbenv
git clone https://github.com/sstephenson/ruby-build.git ~/.rbenv/plugins/ruby-build

echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bash_profile
echo 'eval "$(rbenv init -)"' >> ~/.bash_profile

. ~/.bash_profile

sudo yum install -y openssl-devel readline-devel zlib-devel

rbenv install 2.4.0
rbenv global 2.4.0

#---------------------------
# vimつづき
#---------------------------

gem install rsense
gem install rubocop
gem install refe2
bitclust setup

sudo yum install -y ctags

# NeoBundleのインストール
curl https://raw.githubusercontent.com/Shougo/neobundle.vim/master/bin/install.sh | sh

