colorscheme morning
hi TabLine guifg=#AAAAAA guibg=#EEEEEE
hi TabLineSel guifg=yellow guibg=brown
hi TabLineFill guifg=#EEEEEE

set guioptions-=e
set guioptions-=m
set guioptions-=T

hi Cursor guibg=Cyan guifg=NONE
hi CursorIM guibg=Yellow guifg=NONE
set nrformats=hex

set tw=0
autocmd BufRead *.txt set tw=0
