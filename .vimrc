
"=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=
" Tips
"=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=
" ペースト時に勝手にインデントされるのを防ぐ
" ----------------------------------------------------------
" "+p
" あるいは
" :set paste
" <C-S-V>
" :set nopaste
" 以下のやり方でもいけるらしいのだが、うまくいかん。
" :a!
" <C-S-V>
" <esc>
"=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=

" デフォルトのインデント設定(タブで4文字)
set tabstop=4
set softtabstop=4
set shiftwidth=4

" ~ファイルの格納先
set directory=~/.vimback
set backupdir=~/.vimback

set number
syntax on
"[検索関係]
"大文字小文字の区別なし
set ignorecase
"検索でファイルの末尾に達したら、先頭へループする
set wrapscan
"検索結果をハイライト
set hlsearch

"<C-a><C-x>での対称設定。16進数を指定
set nf=hex

set ambiwidth=double

" 貼り付け部分の選択
nnoremap gc `[v`]
vnoremap gc :<C-u>normal gc<Enter>
onoremap gc :<C-u>normal gc<Enter>

hi DiffAdd    ctermfg=white ctermbg=2
hi DiffChange ctermfg=white ctermbg=3
hi DiffDelete ctermfg=white ctermbg=6
hi DiffText   ctermfg=white ctermbg=1

" Markdownファイルの設定。拡張子がmdであればMarkdownファイル
" md as markdown, instead of modula2
autocmd BufNewFile,BufRead *.{md,mdwn,mkd,mkdn,mark*} set filetype=markdown

" ここから上はあとで整理する
" ##########################################################
" TODO
"  * neosnippetがわかってないので調査
"    * http://qiita.com/koyopro/items/c473b3c2323501b7891a
"    * http://kazuph.hateblo.jp/entry/2012/11/28/105633
"  * 上の設定の整理
"    * 合わせて~/vim/indent/ruby.vim(だっけ？）にインデント設定を移す
"    * http://vimblog.hatenablog.com/entry/vimrc_set_tab_indent_options
"    * http://qiita.com/mitsuru793/items/2d464f30bd091f5d0fef
"  * NeoBundleは拡張子ごとに読み込むかどうか切り替えるようにする

" ----------------------------------------------------------
" NeoBundle
" ----------------------------------------------------------
" 参考：http://kaworu.jpn.org/vim/NeoBundle
" プラグインを追加したい: NeoBundle行を追加してNeoBundleInstall
" プラグインを削除したい: NeoBundle行を削除して~/.vim/bundle/からプラグインを削除する
" プラグインを更新したい: NeoBundleUpdate

" Ruby関係のプラグインについて参考にしたURL
" http://qiita.com/mogulla3/items/42a7f6c73fa4a90b1df3
" http://qiita.com/alpaca_taichou/items/ab2ad83ddbaf2f6ce7fb

if &compatible
  set nocompatible               " Be iMproved
endif

" Required:
set runtimepath+=~/.vim/bundle/neobundle.vim/

" Required:
call neobundle#begin(expand('~/.vim/bundle'))

" Let NeoBundle manage NeoBundle
" Required:
NeoBundleFetch 'Shougo/neobundle.vim'

" Add or remove your Bundles here:
" ----------------------------------------------------------
" コード補完:neocomplete.vim
" ----------------------------------------------------------
" 勝手に補完されるようになる
NeoBundle 'Shougo/neocomplete.vim'
" supermomonga/neocomplete-rsense.vimがなくなってるので使えなくなった
" けど、補完は聞くので、neocomplete本体に取り込まれたとかなのかもしれん。
" gem install rsense
NeoBundle 'marcus/rsense'
" NeoBundle 'supermomonga/neocomplete-rsense.vim'
NeoBundle 'Shougo/neosnippet.vim'
NeoBundle 'Shougo/neosnippet-snippets'

let g:acp_enableAtStartup = 0
let g:neocomplete#enable_at_startup = 1
let g:neocomplete#enable_smart_case = 1
if !exists('g:neocomplete#force_omni_input_patterns')
  let g:neocomplete#force_omni_input_patterns = {}
endif
let g:neocomplete#force_omni_input_patterns.ruby = '[^.*\t]\.\w*\|\h\w*::'

" さっぱりわからんけど、neosnippetの設定
" https://github.com/Shougo/neosnippet.vimからコピペ
" ============================================================
" Plugin key-mappings.
" Note: It must be "imap" and "smap".  It uses <Plug> mappings.
imap <C-k>     <Plug>(neosnippet_expand_or_jump)
smap <C-k>     <Plug>(neosnippet_expand_or_jump)
xmap <C-k>     <Plug>(neosnippet_expand_target)

" SuperTab like snippets behavior.
" Note: It must be "imap" and "smap".  It uses <Plug> mappings.
imap <C-k>     <Plug>(neosnippet_expand_or_jump)
"imap <expr><TAB>
" \ pumvisible() ? "\<C-n>" :
" \ neosnippet#expandable_or_jumpable() ?
" \    "\<Plug>(neosnippet_expand_or_jump)" : "\<TAB>"
smap <expr><TAB> neosnippet#expandable_or_jumpable() ?
\ "\<Plug>(neosnippet_expand_or_jump)" : "\<TAB>"

" For conceal markers.
if has('conceal')
  set conceallevel=2 concealcursor=niv
endif
" neosnippetの設定ここまで
" ============================================================

" 独自スニペットのパス
let g:neosnippet#snippets_directory = '~/.vim/snippets/'

" ----------------------------------------------------------
" 静的解析:rubocop
" ----------------------------------------------------------
" gem install rubocop
" 保存時に勝手に静的解析されるようになる
NeoBundle 'scrooloose/syntastic'
" syntastic_mode_mapをactiveにするとバッファ保存時にsyntasticが走る
" active_filetypesに、保存時にsyntasticを走らせるファイルタイプを指定する
let g:syntastic_mode_map = { 'mode': 'passive', 'active_filetypes': ['ruby'] }
let g:syntastic_ruby_checkers = ['rubocop']

" ----------------------------------------------------------
" ドキュメント参照
" ----------------------------------------------------------
" 参照したいメソッドなどの上でKを入力することで該当するメソッドの一覧が出てくる
" :Ref refe Array#slice や :Ref ri Array#sliceのように見たいものを指定することも可能
" gem install refe2
NeoBundle 'thinca/vim-ref'
NeoBundle 'yuku-t/vim-ref-ri'

" ----------------------------------------------------------
" メソッド定義元へのジャンプ
" ----------------------------------------------------------
" sudo apt-get install ctags
" 参考: http://k0kubun.hatenablog.com/entry/2013/11/02/103400
" :TagsGenerate!でタグを生成する
" メソッド名などの上で<C-]>で定義に飛ぶ。<C-o>で戻る。<C-i>は<C-o>の逆。
" タグファイルはルートディレクトリ/tagsか.git/tagsが作成される
NeoBundle 'szw/vim-tags'

" ----------------------------------------------------------
" Rails
" ----------------------------------------------------------
" Rmodel userのようにすればUserモデルを開ける
NeoBundle "tpope/vim-rails"

" ----------------------------------------------------------
" ファイルをTree表示
" ----------------------------------------------------------
" :NERDTreeで起動
NeoBundle 'scrooloose/nerdtree'

" ----------------------------------------------------------
" vimからgitを使う
" ----------------------------------------------------------
" GgrepやGblemeなどGから始まるコマンドが定義される
NeoBundle 'tpope/vim-fugitive'
" grep検索の実行後にQuickFix Listを表示する
autocmd QuickFixCmdPost *grep* cwindow

" ステータス行に現在のgitブランチを表示する
set statusline+=%{fugitive#statusline()}

" ----------------------------------------------------------
" シングルクォートとダブルクォートの入れ替え
" ----------------------------------------------------------
" cs'"で'=>"、cs"'で"=>'
" ()などにも対応しているようだ
NeoBundle 'tpope/vim-surround'

" ----------------------------------------------------------
" インデントに色を付け
" ----------------------------------------------------------
" vimrcの設定だけで有効化される
" E411: ハイライトグループが見つかりません: Normalを抑止するため以下を指定
colorscheme default
NeoBundle 'nathanaelkane/vim-indent-guides'
let g:indent_guides_enable_on_vim_startup = 1 " 有効化
" 参考元:http://wonderwall.hatenablog.com/entry/2016/03/21/205741
" 奇数行、偶数行ごとの色を変更
let g:indent_guides_auto_colors = 0
autocmd VimEnter,Colorscheme * :hi IndentGuidesOdd  guibg=red   ctermbg=3
autocmd VimEnter,Colorscheme * :hi IndentGuidesEven guibg=green ctermbg=4
" 色を付けるのをスペース1文字にする
let g:indent_guides_guide_size = 1
" 色を付けるのを1段目のインデントからにする
let g:indent_guides_start_level = 1

" ----------------------------------------------------------
" ログファイルに色付け
" ----------------------------------------------------------
" :AnsiEscで表示をトルグ
" 試したい場合は sudo dmesg --color=always > fileでfileを表示してみればよい
NeoBundle 'vim-scripts/AnsiEsc.vim'

" ----------------------------------------------------------
" 行末の半角スペースを可視化
" ----------------------------------------------------------
" デフォルトで機能が有効になる
NeoBundle 'bronson/vim-trailing-whitespace'


" ----------------------------------------------------------
" Markdown
" ----------------------------------------------------------
" Markdown用の便利機能を提供
NeoBundle 'plasticboy/vim-markdown'
" plasticboy/vim-markdownで:TableFormatを使えるようにするのに必要
NeoBundle 'godlygeek/tabular'
" ブラウザでのプレビュー
" プレビューを開くには:PrevimOpenを実行する
NeoBundle 'kannokanno/previm'
NeoBundle 'tyru/open-browser.vim'
" 折りたたみさせない
let g:vim_markdown_folding_disabled=1

au BufRead,BufNewFile *.md set filetype=markdown

" ------------------
" デフォルトのプラグイン
" ------------------
" NeoBundle 'ctrlpvim/ctrlp.vim'
" NeoBundle 'flazz/vim-colorschemes'

" You can specify revision/branch/tag.
" NeoBundle 'Shougo/vimshell', { 'rev' : '3787e5' }
" ------------------
" デフォルトのプラグイン ここまで
" ------------------

" Required:
call neobundle#end()

" ----------------------------------------------------------
" Rsense
" supermomonga/neocomplete-rsense.vimがなくなってるので使えなくなった
" ----------------------------------------------------------
" let g:rsenseHome = '/usr/local/lib/rsense-0.3'
" let g:rsenseUseOmniFunc = 1

" ----------------------------------------------------------
" 基本設定
" ----------------------------------------------------------
" vim内部で使われる文字エンコーディングをutf-8に設定する
set encoding=utf-8

" 想定される改行コードの指定する
set fileformats=unix,dos,mac

" ハイライトを有効化する
syntax enable

" 新しい行を開始したとき、新しい行のインデントを現在行と同じにする
" set autoindent

" def...endの対応をする
" 参考元：http://nanasi.jp/articles/vim/matchit_vim.html
runtime macros/matchit.vim
let b:match_words = "def:end,class:end"

" ファイル形式の検出の有効化する
" ファイル形式別プラグインのロードを有効化する
" ファイル形式別インデントのロードを有効化する
" Required:
filetype plugin indent on

" If there are uninstalled bundles found on startup,
" this will conveniently prompt you to install them.
NeoBundleCheck

