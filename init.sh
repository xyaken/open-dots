#!/bin/bash

for file in .exrc .gemrc .gitconfig .gitignore_global .gvimrc .rubocop.yml .tmux.conf .vimrc
do
  ln -s ~/open-dots/$file ~/$file
done

echo '. ~/open-dots/.bash_profile' >> ~/.bash_profile

mkdir ~/.vimback
mkdir ~/.vim
ln -s ~/open-dots/vim/ftplugin ~/.vim/ftplugin
ln -s ~/open-dots/vim/snipetts ~/.vim/snipetts
